use reqwest::{Client};
use webcrawler::{ResponseSet, create_writer, harvest_links, harvest_images};
use std::env;
use std::io::{Read, Write};

/**
 * Read an url given as command line argument
 * and search the underlying html document for url's
 * and images. Store the images and urls in a json 
 * object and save to file.
 * 
 * Example use: `webcrawler nos.nl` or
 * `cargo run nos.nl` (in development mode)
 * 
 */
fn main() {
    let args: Vec<String> = env::args().collect();
    let domain_name = &args[1];
    let url = format!("https://{}", domain_name);
    let client = Client::new();
    let mut res = client.get(&url).send().unwrap();

    println!("Parsing: {} Status: {}", url, res.status());

    let mut writer = create_writer(&domain_name);
    let mut body = String::new();
    res.read_to_string(&mut body).unwrap();

    let result = ResponseSet {
        urls: harvest_links(&body, &url).to_owned(),
        images: harvest_images(&body, &url).to_owned(),
    };
    
    serde_json::to_writer(&mut writer, &result).unwrap();
    writer.flush().unwrap();
    println!("...Finished");
}