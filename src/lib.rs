use chrono;
use reqwest::{Url};
use std::collections::HashSet;
use std::fs::File;
use std::io::{BufWriter};
use select::document::Document;
use select::predicate::Name;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct ResponseSet {
    pub urls: Vec<String>,
    pub images: Vec<String>,
}

/**
 * If a url is not fully qualified, prefix it with the
 * given domain url.
 */
pub fn normalize(url: &str, domain: &str) -> Option<String> {
    let new_url = Url::parse(url);
    match new_url {
        Ok(new_url) => {
            if new_url.has_host() {
                Some(url.to_string())
            } else {
                None
            }
        },
        Err(_e) => {
            // Relative urls are not parsed by Reqwest
            if url.starts_with('/') {
                Some(format!("{}{}", domain, url))
            } else {
                None
            }
        }
    }
}

/**
 * Harvest a html document for the given node_type's attribute value.
 * Returns all results appending the domain name if the result starts
 * with a slash
 */
fn harvest(doc: &str, domain: &str, node_type: &str, attribute: &str) -> Vec<String> {
    Document::from(doc)
    .find(Name(node_type))
    .filter_map(|node|node.attr(attribute))
    .filter_map(|node| normalize(node, domain))
    .collect::<HashSet<String>>().into_iter().collect()
}

/**
 * Use the harvest function to gather all links
 */
pub fn harvest_links(doc: &str, domain: &str) -> Vec<String> {
    harvest(doc, domain, "a", "href")
}

/**
 * Use the harvest function to gather all image url's
 */
pub fn harvest_images(doc: &str, domain: &str) -> Vec<String> {
    harvest(doc, domain, "img", "src")
}

/**
 * Create a file and return a writer so results can be written 
 * in other functions
 */
pub fn create_writer(domain_name: &str) -> BufWriter<File>{
    let dt = chrono::Local::now();
    let filename = format!("{}_{}.json", domain_name, dt.format("%Y-%m-%d_%H.%M.%S"));
    let file = File::create(&filename).unwrap();
    BufWriter::new(file)
}