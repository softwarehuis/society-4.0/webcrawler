extern crate webcrawler;
#[cfg(test)]
mod tests {
    use webcrawler::normalize;

    #[test]
    fn test_normalize() {
        assert!(normalize("#test", "https://example.com").is_none());
        assert_eq!(normalize("/", "https://example.com"),
            Some(String::from("https://example.com/")));
        assert_eq!(normalize("https://example.com/test/page.html", "https://example.com"),
            Some(String::from("https://example.com/test/page.html")));
    }
}